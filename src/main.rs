mod file;
mod shared;
mod stdin;

use clap::{App, Arg};
use std::path::PathBuf;

fn main() {
    let matches = App::new("lolcat-rs")
        .version("180419")
        .author("David Bittner <bittneradave@gmail.com>")
        .about("cat, but with rainbow text!")
        .arg(
            Arg::with_name("PATH")
                .help("The path of the file to cat")
                .required(false)
                .multiple(true),
        )
        .get_matches();

    if let Some(args) = matches.values_of("PATH") {
        let args: Vec<_> = args.map(|x| PathBuf::from(x.to_owned())).collect();

        match file::stream_files(args) {
            Err(err) => eprintln!("error occured: {}", err),
            _ => (),
        }
    } else {
        match stdin::parse_stdin() {
            Err(err) => eprintln!("error occured: {}", err),
            _ => (),
        }
    }
}
