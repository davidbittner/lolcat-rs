use std::error::Error;
use std::io::*;
use termcolor::*;
use ansi_parser::{AnsiParser, Output::*};

type Result<T> = std::result::Result<T, Box<dyn Error>>;

pub struct WrappedLine {
    data: String,
    line_no: u64,
}

impl WrappedLine {
    pub fn new(data: String, line_no: u64) -> Self {
        WrappedLine {
            data: data.to_string(),
            line_no: line_no,
        }
    }

    pub fn write<T: WriteColor + Write>(&self, stream: &mut T) -> Result<()> {
        let mut spec = ColorSpec::new();
        let parse: Vec<_> = self.data
            .ansi_parse()
            .collect();

        let mut char_no = 0;

        for block in parse {
            match block {
                TextBlock(text) => {
                    for ch in text.chars() {
                        char_no+=1;
                        let (r, g, b) = calc_color(self.line_no, char_no);
                        let col = Color::Rgb(r, g, b);

                        stream.set_color(spec.set_fg(Some(col)))?;
                        write!(stream, "{}", ch)?;
                    }
                },
                Escape(seq) => write!(stream, "{}", seq)?
            }
        }

        write!(stream, "\n")?;
        Ok(())
    }
}

fn calc_color(line_no: u64, char_no: u64) -> (u8, u8, u8) {
    let offset = std::f64::consts::PI / 3.0;

    let ind   = (line_no as f64) / 15.0 + ((char_no as f64) / 20.0);

    let red   = ((ind as f64).sin() * 127.0) + 128.0;
    let green = ((ind as f64 + (offset * 2.0)).sin() * 127.0) + 128.0;
    let blue  = ((ind as f64 + (offset * 4.0)).sin() * 127.0) + 128.0;

    (red as u8, green as u8, blue as u8)
}
