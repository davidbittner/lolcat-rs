use std::fs::File;
use std::io::*;
use std::path::PathBuf;
use termcolor::*;

use std::error::Error;

use super::shared::WrappedLine;

type Result<T> = std::result::Result<T, Box<dyn Error>>;

pub fn stream_file(path: PathBuf, mut line_no: u64) -> Result<u64> {
    let path = path.canonicalize()?;
    let file = BufReader::new(File::open(path)?);

    let mut stream = StandardStream::stdout(ColorChoice::AlwaysAnsi);
    stream.lock();

    for line in file.lines() {
        let wrapped = WrappedLine::new(line?, line_no);
        line_no += 1;

        wrapped.write(&mut stream)?;
    }

    Ok(line_no)
}

pub fn stream_files(files: Vec<PathBuf>) -> Result<()> {
    let mut line_no = rand::random::<u16>() as u64;

    for file in files.into_iter() {
        line_no = stream_file(file, line_no)?;
    }

    Ok(())
}
