use super::shared::WrappedLine;
use std::error::Error;
use std::io::BufRead;
use termcolor::*;

type Result<T> = std::result::Result<T, Box<dyn Error>>;

pub fn parse_stdin() -> Result<Vec<()>> {
    let mut out = StandardStream::stdout(ColorChoice::AlwaysAnsi);
    let adder = rand::random::<u16>() as u64;

    std::io::stdin()
        .lock()
        .lines()
        .enumerate()
        .map(|(no, line)| WrappedLine::new(line?, adder + no as u64).write(&mut out))
        .collect()
}
